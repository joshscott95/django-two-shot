from django import forms
from .models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)
        if user:
            self.fields["category"].queryset = ExpenseCategory.objects.filter(
                owner=user
            )
            self.fields["account"].queryset = Account.objects.filter(
                owner=user
            )

    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]

        def form_valid(self, form):
            form.instance.purchaser = self.request.user
            return super().form_valid(form)


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
