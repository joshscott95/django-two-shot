from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def create_account_view(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "accounts/create/create_account.html", context)


@login_required
def create_category_view(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}
    return render(request, "categories/create/create_category.html", context)


@login_required
def account_list_view(request):
    accounts = Account.objects.filter(
        receipts__purchaser=request.user
    ).distinct()
    account_info = []
    for account in accounts:
        receipt_count = Receipt.objects.filter(account=account).count()
        account_info.append((account.name, account.number, receipt_count))

    context = {"account_info": account_info}
    return render(request, "accounts/account_list.html", context)


@login_required
def category_list_view(request):
    categories = ExpenseCategory.objects.filter(
        receipts__purchaser=request.user
    ).distinct()
    category_counts = []
    for category in categories:
        count = Receipt.objects.filter(category=category).count()
        category_counts.append((category, count))

    context = {"category_counts": category_counts}
    return render(request, "categories/category_list.html", context)


@login_required
def home(request):
    return render(request, "receipt_list.html")


@login_required
def create_receipt_view(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST, user=request.user)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm(user=request.user)

    context = {"form": form}
    return render(request, "create_receipt.html", context)


@login_required
def receipt_list_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipt_list.html", context)
